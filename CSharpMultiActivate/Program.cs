﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading; // Mutexを使用するため追加します。

namespace CSharpMultiActivate
{
    static class Program
    {
        // Mutexの名前
        private static readonly string name = "SampleApp";

        [STAThread]
        static void Main()
        {
            // Mutexを生成します。
            var createdNew = false;
            var mutex = new Mutex(true, name, out createdNew);

            // Mutexの初期所有権が無い場合、すでに起動済みとみなします。
            if (!createdNew)
            {
                MessageBox.Show("すでに起動済みです。");
                mutex.Close();
                return;
            }

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }

            // Mutexのクリア処理を行います。
            finally
            {
                mutex.ReleaseMutex();
                mutex.Close();
            }
        }
    }
}
